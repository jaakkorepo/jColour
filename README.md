# jColour

Website + discord.js interface for the jColour bot

[![Discord Bots](https://discordbots.org/api/widget/358968052500660226.svg)](https://discordbots.org/bot/358968052500660226)

## Invite me!

https://jcolour.jaqreven.com/invite

## Installing

* Fork the repository
* Clone it
* Install the necessary components (Ejs, Express etc) with `npm i`
* Make a Discord bot
* Rename /config/config.json.example to /config/config.json and fill it up
* npm start 

* (Ex. Apache for domains)


## Built With

* [Bulma](https://bulma.io) - The css framework used
* [Express](https://expressjs.com/) - Web application framework
* [EJS](http://ejs.co/) - Javascript templating
* [Discord.js](https://discord.js.org) - Doing the heavy lifting for the Discord API.
* [Commando](https://github.com/discordjs/Commando) - Command extension for Discord.js

## Contributing

Want to contribute? That would be great.
We have a few guidelines and would appreciate you following them.

* Code must be supported on Firefox, Chrome, Safari, Edge and IE if possible.
* If possible, no heavy libraries such as JQuery.
* Please format your code like the rest in our project.
* Please add comments to your code. 


Please submit a pull request or an issue.

## Authors

See the list of [contributors](https://github.com/jaqreven/jColour/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
